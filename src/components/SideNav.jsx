import { NavListData } from "../data/data";

const SideNav = ({ closeModal }) => {
  return (
    <>
      <div className="w-full md:max-w-sm h-full fixed top-0 right-0 bg-[#505050] z-50">
        <button
          onClick={closeModal}
          type="button"
          className=" absolute top-3 left-3 w-10 h-10"
        >
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path
              fill="#fff"
              d="M256 48a208 208 0 1 1 0 416 208 208 0 1 1 0-416zm0 464A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM175 175c-9.4 9.4-9.4 24.6 0 33.9l47 47-47 47c-9.4 9.4-9.4 24.6 0 33.9s24.6 9.4 33.9 0l47-47 47 47c9.4 9.4 24.6 9.4 33.9 0s9.4-24.6 0-33.9l-47-47 47-47c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0l-47 47-47-47c-9.4-9.4-24.6-9.4-33.9 0z"
            />
          </svg>
        </button>
        <ul className="flex justify-between items-center flex-col mt-24">
          {NavListData.map((items, index) => {
            const { navitems, pathLink } = items;
            return (
              <li key={index} className="nav_list group">
                <a
                  onClick={closeModal}
                  className="nav_link group-hover:text-brand-secondary"
                  href={pathLink}
                >
                  {navitems}
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    </>
  );
};

export default SideNav;
