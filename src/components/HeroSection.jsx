import TopNavigation from "./TopNavigation";

const HeroSection = () => {
  return (
    <>
      <div className="w-full h-[100dvh]  bg-[url(/home-top-bg.jpg)] bg-no-repeat relative lg:mb-[200px]">
        <TopNavigation />
        <div
          id="home"
          className="mx-auto text-white text-center max-w-3xl w-full px-5 pt-28 target_section"
        >
          <p className="text-xl font-medium mb-4">Bdtask Media</p>
          <h1 className="font-extrabold text-xl md:text-4xl tracking-wide md:leading-normal">
            Digital Content, Animation & Video Editing Service Provider in
            Bangladesh.
          </h1>
          <div className="mt-8">
            <button
              type="button"
              className="uppercase bg-brand-primary text-white mr-4 px-6 py-3 rounded-lg"
            >
              HIRE US NOW
            </button>
            <button
              type="button"
              className="border border-brand-primary px-6 py-3 rounded-lg"
            >
              Contact Us
            </button>
          </div>
          <div className="bg-white p-3 rounded-3xl shadow-lg w-[320px] lg:w-full lg:max-w-2xl 2xl:max-w-3xl relative mx-auto mt-12">
            <img
              className="w-full h-full object-cover"
              src="/video.jpg"
              alt=""
            />

            <button
              type="button"
              className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2"
            >
              <svg
                width="70"
                height="70"
                viewBox="0 0 70 70"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <circle cx="35" cy="35" r="35" fill="#16994A" />
                <path
                  d="M45.5 31.9019C47.5 33.0566 47.5 35.9434 45.5 37.0981L32.75 44.4593C30.75 45.614 28.25 44.1706 28.25 41.8612L28.25 27.1388C28.25 24.8294 30.75 23.386 32.75 24.5407L45.5 31.9019Z"
                  fill="white"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default HeroSection;
