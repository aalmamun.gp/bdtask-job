import { useEffect } from "react";
import ReactDOM from "react-dom";

const CustomModal = ({ closeModal, children }) => {
  useEffect(() => {
    document.body.style.overflowY = "hidden";
    return () => {
      document.body.style.overflowY = "scroll";
    };
  }, []);
  return ReactDOM.createPortal(
    <>
      <div
        onClick={closeModal}
        className="w-full h-screen fixed top-0 left-0 bg-dark-primary/80 z-50"
      ></div>
      {children}
    </>,
    document.querySelector("#myPortalModal")
  );
};

export default CustomModal;
