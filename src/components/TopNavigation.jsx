import { useState, useEffect } from "react";

import { NavListData } from "../data/data";
import CustomModal from "./CustomModal";
import SideNav from "./SideNav";

/* const NavListData = [
  {
    id: 1,
    navitems: "Home",
    pathLink: "#home",
  },
  {
    id: 2,
    navitems: "Services",
    pathLink: "#services",
  },
  {
    id: 3,
    navitems: "Portfolio",
    pathLink: "#portfolio",
  },
  {
    id: 4,
    navitems: "Clients",
    pathLink: "#clients",
  },
  {
    id: 5,
    navitems: "Review",
    pathLink: "#review",
  },
]; */

const TopNavigation = () => {
  const [fix, setFix] = useState(false);
  const [showSideNav, setShowSideNav] = useState(false);
  function closeSideNav() {
    setShowSideNav(false);
  }

  function setTopFixed() {
    if (window.scrollY >= 100) {
      setFix(true);
    } else {
      setFix(false);
    }
  }

  useEffect(() => {
    window.addEventListener("scroll", setTopFixed);
  });

  useEffect(() => {
    const sections = [...document.querySelectorAll(".target_section")];
    const link = (id) => document.querySelector(`a[href="#${id}"]`);

    const inView = (element) => {
      var top = element.offsetTop;
      var height = element.offsetHeight;

      while (element.offsetParent) {
        element = element.offsetParent;
        top += element.offsetTop;
      }

      return (
        top < window.pageYOffset + window.innerHeight &&
        top + height > window.pageYOffset + 125
      );
    };

    const init = () => {
      function update() {
        let next = false;

        sections.forEach((sections) => {
          if (link(sections.id)) {
            const current = link(sections.id);

            if (current) {
              if (inView(sections) && !next) {
                current.classList.add("active_link");
                next = true;
              } else {
                current.classList.remove("active_link");
              }
            }
          }
        });
      }
      update();
      window.addEventListener("scroll", update);
    };

    init();
  }, []);
  return (
    <>
      <header
        className={`top-0 left-0 w-full z-50 h-[90px] ${
          fix
            ? "bg-dark-primary/80 backdrop-blur-sm shadow-sm shadow-brand-primary fixed  transition-all duration-500 ease-in-out"
            : "bg-dark-primary/0  absolute  transition-all duration-500 ease-in-out"
        }`}
      >
        <nav className="px-4 md:px-8 xl:px-0 py-5 flex justify-between  items-center  container_section ">
          <a href="/">
            <img
              className="hover:opacity-80  "
              src="/brand-logo.svg"
              alt="logo"
            />
          </a>

          <div className="items-center hidden  lg:flex">
            <ul className="mr-12">
              {NavListData.map((items, index) => {
                const { navitems, pathLink } = items;
                return (
                  <li key={index} className="nav_list group">
                    <a
                      className="nav_link group-hover:text-brand-secondary"
                      href={pathLink}
                    >
                      {navitems}
                    </a>
                  </li>
                );
              })}
            </ul>
            <div className="bg-brand-primary px-2 py-1 flex items-center rounded-full">
              <svg
                width="45"
                height="43"
                viewBox="0 0 45 43"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <ellipse
                  cx="22.2401"
                  cy="21.6313"
                  rx="22.2171"
                  ry="21.205"
                  fill="white"
                />
                <path
                  d="M32.1148 27.5977C32.1148 27.9369 32.0357 28.2856 31.8676 28.6249C31.6994 28.9642 31.4819 29.2846 31.1951 29.5862C30.7105 30.0951 30.1765 30.4627 29.5733 30.6983C28.9799 30.9339 28.3371 31.0564 27.6449 31.0564C26.6362 31.0564 25.5583 30.8302 24.421 30.3684C23.2838 29.9067 22.1466 29.2846 21.0192 28.5024C19.8819 27.7108 18.804 26.8343 17.7756 25.8636C16.757 24.8834 15.8373 23.8561 15.0165 22.7818C14.2056 21.7074 13.5529 20.633 13.0782 19.568C12.6035 18.4936 12.3662 17.4664 12.3662 16.4862C12.3662 15.8454 12.4849 15.2328 12.7222 14.6673C12.9596 14.0924 13.3353 13.5646 13.8595 13.0934C14.4924 12.4997 15.1846 12.2075 15.9164 12.2075C16.1933 12.2075 16.4702 12.2641 16.7174 12.3772C16.9745 12.4903 17.202 12.6599 17.38 12.9049L19.6743 15.9867C19.8523 16.2223 19.9808 16.4391 20.0698 16.6464C20.1588 16.8444 20.2083 17.0423 20.2083 17.2213C20.2083 17.4475 20.1391 17.6737 20.0006 17.8905C19.8721 18.1072 19.6842 18.3334 19.4468 18.5596L18.6952 19.3041C18.5865 19.4078 18.537 19.5303 18.537 19.6811C18.537 19.7565 18.5469 19.8225 18.5667 19.8979C18.5964 19.9733 18.626 20.0298 18.6458 20.0864C18.8238 20.3974 19.1304 20.8026 19.5655 21.2927C20.0105 21.7828 20.4852 22.2823 20.9994 22.7818C21.5334 23.2813 22.0477 23.7431 22.5718 24.1672C23.086 24.5818 23.5112 24.8646 23.8475 25.0342C23.8969 25.0531 23.9563 25.0813 24.0255 25.1096C24.1046 25.1379 24.1837 25.1473 24.2727 25.1473C24.4408 25.1473 24.5694 25.0908 24.6782 24.9871L25.4297 24.2803C25.677 24.0446 25.9143 23.8656 26.1418 23.7525C26.3692 23.6205 26.5967 23.5546 26.8439 23.5546C27.0318 23.5546 27.2296 23.5923 27.4471 23.6771C27.6647 23.7619 27.8921 23.8844 28.1394 24.0446L31.4127 26.2594C31.6698 26.429 31.8478 26.6269 31.9566 26.8626C32.0554 27.0982 32.1148 27.3338 32.1148 27.5977Z"
                  stroke="#00A653"
                  strokeWidth="2"
                  strokeMiterlimit="10"
                />
                <path
                  opacity="0.4"
                  d="M29.029 18.687C29.029 18.0811 28.5318 17.1522 27.7912 16.3948C27.1141 15.6981 26.2149 15.1528 25.3262 15.1528"
                  stroke="#00A653"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  opacity="0.4"
                  d="M32.1147 18.6868C32.1147 15.1047 29.0793 12.2075 25.3262 12.2075"
                  stroke="#00A653"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
              <p className="text-white font-bold text-base px-2">
                + 888 999 000 111
              </p>
            </div>
          </div>
          <button
            onClick={() => setShowSideNav(true)}
            type="button"
            className="relative group lg:hidden"
          >
            <div className="relative flex items-center justify-center rounded-full w-[40px] h-[40px] lg:w-[45px] lg:h-[45px] transform transition-all bg-dark-primary  ring-gray-300 ring-[2px] md:ring-[3px] ring-opacity-50 duration-200 shadow-md  md:active:ring-[3px]">
              <div className="flex flex-col justify-between w-[20px] h-[20px] transform transition-all duration-300 origin-center">
                <div className="h-[2px] w-1/2 rounded transform transition-all duration-300 origin-right delay-75 bg-brand-primary"></div>

                <div className="h-[2px] rounded bg-brand-primary"></div>

                <div className="h-[2px] w-1/2 rounded self-end transform transition-all duration-300 origin-left delay-75 bg-brand-primary"></div>
              </div>
            </div>
          </button>
        </nav>
      </header>

      {showSideNav && (
        <CustomModal closeModal={closeSideNav}>
          <SideNav closeModal={closeSideNav} />
        </CustomModal>
      )}
    </>
  );
};

export default TopNavigation;
