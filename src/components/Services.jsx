const ServicesCardData = [
  {
    id: 1,
    img: "card-1.jpg",
    title: "TVC/OVC Digital Content",
  },
  {
    id: 2,
    img: "card-2.jpg",
    title: "Documentary Video NGO & Others Video",
  },
  {
    id: 3,
    img: "card-3.jpg",
    title: "Drone Survey & Aerial Video Graphy Video",
  },
  {
    id: 4,
    img: "card-4.jpg",
    title: "Audio and short Film Video Making",
  },
  {
    id: 5,
    img: "card-3.jpg",
    title: "Documentary Video NGO & Others Video",
  },
  {
    id: 6,
    img: "card-1.jpg",
    title: "TVC/OVC Digital Content",
  },
  {
    id: 7,
    img: "card-4.jpg",
    title: "Drone Survey & Aerial Video Graphy Video",
  },
  {
    id: 8,
    img: "card-2.jpg",
    title: "Audio and short Film Video Making",
  },
];

const Services = () => {
  return (
    <div id="services" className="target_section container_section pt-24">
      <div className="w-full px-4 max-w-xl mx-auto mb-10 text-center">
        <h2
          id="service_container"
          className="font-extrabold text-2xl md:text-4xl "
        >
          Bdtask Media Services
        </h2>
        <p className="mt-3">
          We’re a nimble video production company assisting brands and
          organizations shed light on their accomplishments
        </p>
      </div>
      <div className="items_card_wrapper grid md:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4 gap-4">
        {ServicesCardData.map((items, index) => {
          const { img, title } = items;
          return (
            <div
              key={index}
              className="items_card p-3 bg-[#f6f6f6] border border-[#e3e3e3] rounded-xl"
            >
              <a
                href=""
                className=" text-dark-primary h-full rounded-xl overflow-hidden block"
              >
                <div className="w-full h-[250px] overflow-hidden rounded-xl bg-white p-1">
                  <img
                    src={img}
                    alt="Card img"
                    className="w-full h-full object-cover block rounded-xl"
                  />
                </div>
                <div className="p-6">
                  <h2 className="font-bold text-xl text-center font-bold">
                    {title}
                  </h2>
                </div>
              </a>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Services;
