const OurWorkingProcess = () => {
  return (
    <div className="bg-black py-8 md:py-16 px-4 relative md:mb-24 target_section">
      <div className="w-full px-4 max-w-xl mx-auto mb-10 text-center text-white">
        <h2 className="font-extrabold text-2xl md:text-4xl">
          Our Working Process
        </h2>
        <p className="mt-3">
          Production Process users around the world, love us with great trust
        </p>
      </div>
      <div className="flex justify-center items-center mb-6 md:mb-16">
        <img src="/worke-process.jpg" alt="" />
      </div>
      <div className="bg-brand-primary md:flex items-center justify-between flex-col md:flex-row max-w-2xl lg:max-w-4xl w-full px-4 md:px-8 py-3 text-white rounded-xl md:absolute md:left-1/2 md:-translate-x-1/2 md:-bottom-10 md:h-[80px]">
        <p className="lg:text-md font-bold mb-4 md:mb-0">
          Do you need any help? <br />
          Get in touch with our support team.
        </p>
        <div className="flex items-center">
          <svg
            width="50"
            height="50"
            viewBox="0 0 60 60"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <circle cx="30" cy="30" r="29.5" stroke="white" />
            <g clipPath="url(#clip0_156_1063)">
              <path
                d="M25.2993 32.7727H35.216M25.2993 25.6894H30.2576"
                stroke="white"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M33.3325 41.826C39.2584 41.4321 43.9787 36.6448 44.3671 30.6348C44.4432 29.4587 44.4432 28.2407 44.3671 27.0646C43.9787 21.0546 39.2584 16.2673 33.3325 15.8734C31.3108 15.739 29.2 15.7393 27.1825 15.8734C21.2565 16.2673 16.5362 21.0546 16.1478 27.0646C16.0718 28.2407 16.0718 29.4587 16.1478 30.6348C16.2893 32.8237 17.2573 34.8504 18.397 36.5618C19.0587 37.7598 18.622 39.2552 17.9328 40.5613C17.4358 41.5031 17.1873 41.974 17.3868 42.3141C17.5864 42.6543 18.032 42.6651 18.9233 42.6868C20.686 42.7297 21.8746 42.23 22.8181 41.5343C23.3532 41.1397 23.6208 40.9424 23.8052 40.9197C23.9896 40.897 24.3525 41.0465 25.0782 41.3454C25.7304 41.614 26.4877 41.7798 27.1825 41.826C29.2 41.9601 31.3108 41.9604 33.3325 41.826Z"
                stroke="white"
                strokeWidth="2"
                strokeLinejoin="round"
              />
            </g>
            <defs>
              <clipPath id="clip0_156_1063">
                <rect
                  width="34"
                  height="34"
                  fill="white"
                  transform="translate(13 15)"
                />
              </clipPath>
            </defs>
          </svg>
          <p className="ms-3 lg:text-md font-bold">support@bdtaskmedia.com</p>
        </div>
      </div>
    </div>
  );
};

export default OurWorkingProcess;
