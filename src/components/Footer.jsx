const Footer = () => {
  return (
    <div
      id="review"
      className="mt-12 bg-black relative overflow-hidden isolate target_section"
    >
      <img
        style={{ width: 300 }}
        className="absolute hidden md:block -z-10 rotate-[145deg] -left-10 lg:top-20 lg:-left-28 lg:rotate-90"
        src="/shape-right.png"
        alt=""
      />
      <img
        style={{ width: 300 }}
        className="absolute hidden md:block -z-10 rotate-[-25deg]  lg:rotate-0 bottom-0 -right-10 lg:bottom-0 lg:right-0"
        src="/shape-right.png"
        alt=""
      />

      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 container_section gap-6  py-8 md:py-12">
        <div className="">
          <img src="/brand-logo.svg" alt="" />
          <p className="text-white mt-3">
            Custom software development conveys “personalizing it specifically
            only for that dedicated entrepreneur or organization”. Creating,
            building.
          </p>
        </div>
        <div className="flex md:justify-center ">
          <ul className="font-bold text-lg text-white">
            Company
            <li className="footer_link"> About us </li>
            <li className="footer_link">Blog </li>
            <li className="footer_link">Careers </li>
            <li className="footer_link">Contact</li>
          </ul>
        </div>
        <div className="flex lg:justify-center">
          <ul className="font-bold text-lg text-white">
            Resource
            <li className="footer_link"> Downloads </li>
            <li className="footer_link">Help Center </li>
            <li className="footer_link">Partners </li>
            <li className="footer_link">Press Kit</li>
          </ul>
        </div>
        <div className="flex md:justify-center">
          <ul className="font-bold text-lg text-white">
            Social Media
            <li className="footer_link"> Facebook </li>
            <li className="footer_link">Twitter </li>
            <li className="footer_link">Instagram </li>
            <li className="footer_link">Linkedin</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Footer;
