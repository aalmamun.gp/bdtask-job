import { useState } from "react";
import CustomModal from "../components/CustomModal";

const About = () => {
  const [myModal, setMyModal] = useState(false);
  return (
    <>
      <div className="h-96 w-full bg-yellow-700 p-6">
        <h2 className="text-5xl">About Page</h2>
        <button
          onClick={() => setMyModal(true)}
          type="button"
          className="font-bold text-3xl bg-white p-3 mt-4"
        >
          Open
        </button>
      </div>
      {myModal && (
        <CustomModal>
          <div className="w-full max-w-3xl min-h-[350px] bg-white p-6 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 z-50 ">
            <h2 className="  ">Abdu</h2>
          </div>
        </CustomModal>
      )}
    </>
  );
};

export default About;
