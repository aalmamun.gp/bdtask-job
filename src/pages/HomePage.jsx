import Footer from "../components/Footer";
import HeroSection from "../components/HeroSection";
import OurWorkingProcess from "../components/OurWorkingProcess";
import Services from "../components/Services";

const HomePage = () => {
  return (
    <>
      <HeroSection />
      <Services />
      <OurWorkingProcess />
      <Footer />
    </>
  );
};

export default HomePage;
