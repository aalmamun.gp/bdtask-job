export const NavListData = [
  {
    id: 1,
    navitems: "Home",
    pathLink: "#home",
  },
  {
    id: 2,
    navitems: "Services",
    pathLink: "#services",
  },
  {
    id: 3,
    navitems: "Portfolio",
    pathLink: "#portfolio",
  },
  {
    id: 4,
    navitems: "Clients",
    pathLink: "#clients",
  },
  {
    id: 5,
    navitems: "Review",
    pathLink: "#review",
  },
];
